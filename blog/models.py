from django.db import models
from django.shortcuts import reverse
from django.utils import timezone

class Category(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('Ссылка', unique=True)
    image = models.ImageField('Картинка', null=True, blank=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
    
    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug':self.slug})

    def __str__(self):
        return self.title

class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('Ссылка', unique=True)
    category = models.ForeignKey(Category, on_delete = models.CASCADE, null=True, verbose_name="Категория")
    summary = models.TextField('Краткое описание')
    text = models.TextField('Описание')
    views = models.IntegerField('Просмотры', default=0)
    image = models.ImageField('Картинка', null=True, blank=True)
    date = models.DateTimeField('Дата', default = timezone.now)

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"

    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug':self.slug})

    def __str__(self):
        return self.title

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name='Пост')
    author_name = models.CharField('Имя автора', max_length=255)
    comment_text = models.TextField('Текст комментария')
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return self.author_name

class FeedBack(models.Model):
    name = models.CharField('Ф.И.О.', max_length=255)
    phone = models.CharField('Телефон', max_length=255)
    email = models.EmailField('Email')
    text = models.TextField('Сообщение')
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name='Сообщение'
        verbose_name_plural='Сообщения'

    def __str__(self):
        return self.name
